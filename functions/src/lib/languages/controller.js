const languages = require('./languages.json')

function getAllLanguages(req, res) {
    return res.json({
        success: true,
        data: languages
    })
}


function getLanguage(req, res) {
    const languageName = req.params.language
    const language = languages.filter(lang => lang.name === languageName)
    const exists = language.length > 0 

    return res.status(exists ? 200 : 404).json(
        {
            success: exists,
            data: exists? language[0] : "language not found"
        }
    )
}

module.exports = {
    getAllLanguages,
    getLanguage
}